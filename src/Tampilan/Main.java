package Tampilan;
import java.util.*;
import Model.*; //syntax untuk mengimpor package model agar class di dalamnya dapat diakses
import Transaksi.*; //syntax untuk mengimpor package transaksi agar class di dalamnya dapat diakses

public class Main {
public static Karyawan [] karyawans = new Karyawan[20];
//inisialisasi array untuk menampung data karyawan, minimarket hanya dapat menampung 20 karyawan saja
public static Barang [] barangs = new Barang[100];
//inisialisasi array untuk menampung data barang, minimarket dapat menampung hingga 100 jenis barang
public static Member [] members = new Member[100];
//inisialisasi array untuk menampung data member, minimarket dapat menampung hingga 100 member
public static Transaction [] transaksis = new Transaction[1000];
//inisialisasi array untuk menampung data transaksi, dapat menampung hingga 1000 transaksi
public static Keuangan keuangan = new Keuangan();
//inisialisasi objek keuangan yang akan diakses oleh method transaksi dan keuangan

private static boolean kasAwals = false; 
//inisialisasi variabel boolean untuk pengondisian pada saat mengisi kas awal, shg kas awal tidak diisi berkali kali
    public static void main(String[] args) {
        menu(); //memanggil method untuk menampilkan menu
    }

    public static void menu(){
        Scanner sc = new Scanner(System.in);
        boolean loopMenu = true; //variabel boolean yang digunakan untuk loop
        do{ //user akan terus berada di method menu selama user tidak masuk ke method lain atau keluar dari program karen nilai loopMenu tidak berubah
	        System.out.println("\n-------------------------------\n   MINIMARKET MANAGEMENT APP\n-------------------------------");
            System.out.println("1. Akses Data Transaksi");
            System.out.println("2. Akses Data Barang");
            System.out.println("3. Akses Data Karyawan");
            System.out.println("4. Akses Data Keuangan");
            System.out.println("5. Akses Data Member");
            System.out.println("0. Keluar");
            System.out.print("Masukkan pilihan: ");
            int pilih = sc.nextInt();
            switch(pilih){
                case 1: aksesTransaksi(); break;
                case 2: aksesBarang(); break;
                case 3: aksesKaryawan(); break;
                case 4: aksesKeuangan(); break;
                case 5: aksesMember(); break;
                case 0: System.out.println("\nTerima Kasih telah menggunakan aplikasi ini."); System.exit(0); sc.close(); break; 
                default:
                System.out.println("\nPilihan yang Anda masukkan tidak ada dalam daftar, Silakan input ulang.\n"); break;
                //jika user menginput pilihan diluar daftar maka akan muncul tampilan seperti berikut dan karena masih dalam loop shg menu akan ditampilkan kembali
            }
         } while(loopMenu);
         sc.close();
    }

    public static void aksesTransaksi(){
        boolean akses0 = true; //variabel boolean yang digunakan untuk melakukan do-while loop menu transaksi
        int indexTransaksi = 0; //index yang digunakan untuk membantu mengisi index array transaksis
        String displayClient = ""; //variabel untuk menampilkan apakah pelanggan merupakan member atau bukan
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("\n-------------------------------\n   MINIMARKET MANAGEMENT APP\n-------------------------------");
            System.out.println("1. Buat Transaksi");
            System.out.println("2. Cetak Daftar Transaksi");
            System.out.println("0. Kembali");
            System.out.print("Masukkan pilihan: ");
            int pilih = sc.nextInt();
            switch(pilih){
                case 1:
                if (barangs[0] == null && karyawans[0] == null || barangs[0] != null && karyawans[0] == null || barangs[0] == null && karyawans[0] != null) {
                    System.out.println("\nBelum ada barang atau karyawan.");
                    } else {
                    System.out.print("Masukkan tanggal transaksi: ");
                    String a = sc.nextLine(); String tgltransaksi = sc.nextLine();
                    boolean adaKaryawan = false;
                    String namakasir = "";
                    do {
                    System.out.print("Masukkan ID kasir: ");
                    String idkasir = sc.nextLine();
                    for (int i = 0; i < karyawans.length; i++) {
                    if (karyawans[i] != null && idkasir.equalsIgnoreCase(karyawans[i].getIdKaryawan())) {
                    namakasir = karyawans[i].getNamaKaryawan();
                    adaKaryawan = true;
                    break;
                    } else if (i == karyawans.length - 1) {
                    System.out.println("\nKaryawan tidak ditemukan pada sistem. Silakan masukkan ulang\n");
                    }
                    }
                    } while (!adaKaryawan);
                    System.out.print("Masukkan nama pelanggan: ");
                    String namaplgn = sc.nextLine();
                    displayClient = "Non-Member";
                    for (int i = 0; i < members.length; i++) {
                    if (members[i] != null && namaplgn.equalsIgnoreCase(members[i].getNamaPelanggan())) {
                    displayClient = "Member";
                    break;
                    }
                    } int index = 1;
                    double totalhrg = 0.0;
                    boolean found = false;
                    boolean lanjut = true;
                    boolean stokCukup = false;
                    do {
                        System.out.print("Masukkan kode barang " + index +": ");
                        String codebrg = sc.next();
                        for (int i = 0; i < barangs.length; i++) {
                            if (barangs[i] != null && codebrg.equalsIgnoreCase(barangs[i].getKodeBarang())) {
                                double hargabrg;
                                hargabrg = barangs[i].getHargaBarang();
                                found = true;
                                do {
                                System.out.print("Masukkan jumlah barang " + index + ": ");
                                int jmlbrg = sc.nextInt();
                                if (jmlbrg > barangs[i].getStokBarang()) {
                                    System.out.println("Stok tidak mencukupi.");
                                } else {
                                    barangs[i].kurangStok(jmlbrg);
                                    totalhrg += hargabrg * jmlbrg;
                                    keuangan.hitungKas();
                                    stokCukup = true;
                                }
                            }while(!stokCukup);
                                String pilihan;
                                do {
                                    System.out.println("Tambah barang yang dimasukkan dalam transaksi? y/n");
                                    pilihan = sc.next();
                                    switch (pilihan) {
                                        case "y":
                                            break;
                                        case "n":
                                            lanjut = false;
                                            break;
                                        default:
                                            System.out.println("Input tidak ada dalam pilihan, silakan masukkan ulang.");
                                    }
                                } while (!(pilihan.equalsIgnoreCase("y") || pilihan.equalsIgnoreCase("n")));
                            }
                        }
                        if (!found) {
                            System.out.println("\nKode barang tidak terdaftar.\n");
                        }
                    } while (lanjut);
                    
                    Transaction transaksi = new Transaction(tgltransaksi, namakasir, namaplgn, totalhrg);
                    transaksis[indexTransaksi] = transaksi;
                    indexTransaksi++;
            }
                break;

                case 2:
                int index = 1;
                if(transaksis[0] == null){
                    System.out.println("\nTidak ada transaksi sebelumnya.");
                } else { System.out.println("\n----------------------------------\n   DAFTAR TRANSAKSI MINIMARKET\n----------------------------------");
                for(Transaction i : transaksis){
                    if(i == null){
                        break;
                    } else System.out.print(index +". ");
                    System.out.println("Transaksi dari " + displayClient);
                    System.out.println(i.diplayTransaksi());
                    System.out.println("----------------------------------");
                    index++;
                }
            } break;

                case 0: menu(); break;
                default: System.out.println("\nPilihan yang Anda masukkan tidak ada dalam daftar, Silakan input ulang.\n"); aksesTransaksi();
            }
        }while(akses0);
        sc.close();
    }

    public static void aksesBarang(){ //method untuk menampilkan menu dan fitur yang berhubungan dengan barang di minimarket
        boolean aksesA = true;
        int indexBarang = 0;
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("\n-------------------------------\n   MINIMARKET MANAGEMENT APP\n-------------------------------");
            System.out.println("1. Tambah Barang");
            System.out.println("2. Hapus Barang");
            System.out.println("3. Tambah Stok Barang");
            System.out.println("4. Cetak Info Barang");
            System.out.println("0. Kembali");
            System.out.print("Masukkan pilihan: ");
            int pilih = sc.nextInt();
            switch(pilih){
            case 1: //case tambah barang
                boolean match = false; //inisialisasi variabel boolean yg digunakan untuk cek kondisi kode barang
                System.out.print("\nMasukkan kode barang: "); String kodeBarang = sc.next();
                for(Barang i : barangs){
                    if(i != null){
                        if(kodeBarang.equalsIgnoreCase(i.getKodeBarang())){ 
                            match = true; //jika kode barang yang diinput sama dengan kode barang pada barang[i], maka variabel match menjadi true
                        }
                    }
                }
                if(!match){ //jika variabel match sama dengan false, maka user dapat memasukkan data barang
                    System.out.print("Masukkan nama barang: "); String a = sc.nextLine(); String namaBarang = sc.nextLine();
                    System.out.print("Masukkan harga barang: "); double hargaBarang = sc.nextDouble();
                    System.out.print("Masukkan stok barang: "); int stokBarang = sc.nextInt();
                    Barang barang = new Barang(kodeBarang, namaBarang, hargaBarang, stokBarang);
                    barangs[indexBarang] = barang; //data barang yang sudah diinput disimpan dalam variabel array
                    indexBarang++;
                } else System.out.println("\nKode barang sudah terpakai, silakan masukkan kode lain.");
                //jika variabel match sama dengan true maka kode barang yang diinputkan sudah digunakan oleh barang lain, program akan menampilkan menu aksesBarang kembali karena masih di dalam loop
                break;

            case 2: //case hapus barang
            if(barangs[0] == null){
                System.out.println("\nTidak ada barang terdaftar.");
            } else { System.out.print("Masukkan kode barang yang ingin dihapus: "); String kodebarang = sc.next();
            boolean found = false;
            for(int i = 0 ; i < barangs.length ; i++){
                if(barangs[i] != null && kodebarang.equalsIgnoreCase(barangs[i].getKodeBarang())){
                    //jika kode barang yang diinput sama dengan kode barang pada indeks tertentu maka stok barang di index tersebut akan bertambah
                    found = true;
                    System.out.println("\nBarang berhasil dihapus.");
                    for(int j = i ; j < barangs.length - 1 ; j++){
                        barangs[j] = barangs[j+1];
                    } barangs[barangs.length - 1] = null;
                    break;
                }
            } if(!found){
                System.out.printf("\nTidak ada barang dengan kode %s, silakan masukkan ulang.", kodebarang);
            }
            } break;

            case 3: //case tambah stok
            if (barangs[0] == null) {
                System.out.println("\nTidak ada barang terdaftar.");
            } else {
                boolean found = false; // variabel found direset menjadi false
                do {
                    System.out.print("\nMasukkan kode barang yang ingin ditambah: ");
                    String kodeBarangs = sc.next();
                    System.out.print("Masukkan stok tambahan: ");
                    int stokTambah = sc.nextInt();
                    for (Barang barang : barangs) {
                        if (barang != null && kodeBarangs.equalsIgnoreCase(barang.getKodeBarang())) {
                            barang.tambahStok(stokTambah);
                            found = true;
                            System.out.printf("\nStok barang %s telah ditambahkan sebanyak %d.\n", kodeBarangs, stokTambah);
                            break;
                        }
                    }
                    if (!found) {
                        System.out.printf("\nTidak ada barang dengan kode %s, silakan masukkan ulang.\n", kodeBarangs);
                    }
                } while (!found);
            } break;
            


            case 4: //case cetak info barang
                int index = 1;
                if(barangs[0] == null){
                    System.out.println("\nTidak ada barang terdaftar.");
                } else { System.out.println("\n----------------------------------\n   DAFTAR BARANG MINIMARKET\n----------------------------------");
                for(Barang i : barangs){
                    if(i == null){
                        break;
                    } else System.out.print(index +". ");
                    System.out.println(i.displayBarang());
                    System.out.println("----------------------------------");
                    index++;
                }
            } break;

            case 0: menu(); break; //jika user menginput 0 maka program akan kembali menampilkan method menu()
            default:
                System.out.println("\nPilihan yang Anda masukkan tidak ada dalam daftar, Silakan input ulang.\n"); aksesBarang(); //jika input tidak ada dalam daftar maka program akan menampilkan display tersebut dan menampilkan kembali menu aksesBarang
            }
        } while(aksesA);
        sc.close();
    }

    public static void aksesKaryawan(){
        boolean aksesB = true;
        int indexKaryawan = 0;
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("\n-------------------------------\n   MINIMARKET MANAGEMENT APP\n-------------------------------");
            System.out.println("1. Tambah Karyawan");
            System.out.println("2. Hapus Karyawan");
            System.out.println("3. Cetak Info Karyawan");
            System.out.println("0. Kembali");
            System.out.print("Masukkan pilihan: ");
            int pilih = sc.nextInt();
            switch(pilih){
                case 1: 
                    boolean match = false;
                    System.out.print("\nMasukkan ID Karyawan: "); String idKaryawan = sc.next();
                    for(Karyawan i : karyawans){
                        if(i != null){
                        if(idKaryawan.equalsIgnoreCase(i.getIdKaryawan())){
                            match = true;
                            }
                        }
                    }
                    if(match == false){
                        System.out.print("Masukkan nama karyawan: "); String a = sc.nextLine(); String namaKaryawan = sc.nextLine();
                        System.out.print("Masukkan gaji karyawan: "); double gajiKaryawan = sc.nextDouble();
                        System.out.println();
                        Karyawan karyawan = new Karyawan(idKaryawan, namaKaryawan, gajiKaryawan);
                        karyawans[indexKaryawan] = karyawan;
                        indexKaryawan++;
                    } else System.out.println("\nID sudah terpakai, silakan masukkan ID lain.");
                    break;

                case 2:
                if(karyawans[0] == null){
                    System.out.println("\nTidak ada karyawan terdaftar.");
                } else { System.out.print("Masukkan ID karyawan yang ingin dihapus: "); String idkaryawan = sc.next();
                boolean found = false;
                for(int i = 0 ; i < karyawans.length ; i++){
                    if(karyawans[i] != null && idkaryawan.equalsIgnoreCase(karyawans[i].getIdKaryawan())){ 
                        found = true;
                        System.out.println("\nData karyawan berhasil dihapus.");
                        for(int j = i ; j < karyawans.length - 1 ; j++){
                            karyawans[j] = karyawans[j+1];
                        } karyawans[karyawans.length - 1] = null;
                        break;
                    }
                } if(!found){
                    System.out.printf("\nTidak ada karyawan dengan ID %s, silakan masukkan ulang.\n", idkaryawan);
                }
                } break;

                case 3: 
                    int index = 1;
                    if(karyawans[0] == null){
                        System.out.println("\nTidak ada karyawan terdaftar.");
                    } else { System.out.println("\n----------------------------------\n   DAFTAR KARYAWAN MINIMARKET\n----------------------------------");
                    for(Karyawan i : karyawans){
                        if(i == null){
                        break;
                    } else System.out.print(index + ". ");
                    System.out.println(i.displayKaryawan());
                    System.out.println("----------------------------------");
                    index++;
                }
            } break;

                case 0: menu(); break;

                default:
                System.out.println("\nPilihan yang Anda masukkan tidak ada dalam daftar, Silakan input ulang.\n"); aksesKaryawan();
            } 
        } while(aksesB);
        sc.close();
    }
    
    public static void aksesKeuangan(){
        boolean aksesC = true;
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("\n-------------------------------\n   MINIMARKET MANAGEMENT APP\n-------------------------------");
            System.out.println("1. Input kas awal");
            System.out.println("2. Cetak Informasi Kas");
            System.out.println("0. Kembali");
            System.out.print("Masukkan pilihan: ");
            int pilih = sc.nextInt();
            switch(pilih){
                case 1:
                if(!kasAwals){
                System.out.println("\n----------------------------------\n       KAS AWAL MINIMARKET\n----------------------------------");
                System.out.print("Masukkan nominal kas awal: "); double kasawal = sc.nextDouble();
                keuangan.setKasAwal(kasawal);
                kasAwals = true;
                } else System.out.println("\nAnda sudah memasukkan kas awal minimarket."); break;

                case 2:
                System.out.println("\n----------------------------------\n    INFORMASI KAS MINIMARKET\n----------------------------------");
                System.out.printf("Kas awal : %.2f\n", keuangan.getKasAwal());
                System.out.printf("Total kas: %.2f\n", keuangan.hitungKas());
                break;

                case 0: menu(); break;
                default: System.out.println("\nPilihan yang Anda masukkan tidak ada dalam daftar, Silakan input ulang.\n"); aksesKeuangan();
            }
        }while(aksesC);
        sc.close();
    }

    public static void aksesMember(){
        boolean aksesD = true;
        int indexMember = 0;
        Scanner sc = new Scanner(System.in);
        do{
        System.out.println("\n-------------------------------\n   MINIMARKET MANAGEMENT APP\n-------------------------------");
            System.out.println("1. Tambah Member");
            System.out.println("2. Hapus Member");
            System.out.println("3. Cetak Info Member");
            System.out.println("0. Kembali");
            System.out.print("Masukkan pilihan: ");
            int pilih = sc.nextInt();
            switch(pilih){
                case 1:
                boolean match = false;
                    System.out.print("\nMasukkan ID Member: "); String idmember = sc.next();
                    for(Member i : members){
                        if(i != null){
                        if(idmember.equalsIgnoreCase(i.getIdmember())){
                            match = true;
                            }
                        }
                    }
                    if(match == false){
                        System.out.print("Masukkan nama member: "); String a = sc.nextLine(); String namamember = sc.nextLine();
                        System.out.print("Masukkan email member: "); String emailmember = sc.next();
                        System.out.print("Masukkan nomor telepon member: "); String notelp = sc.next();
                        System.out.println();
                        Member member = new Member(namamember, idmember, emailmember, notelp);
                        members[indexMember] = member;
                        indexMember++;
                    } else System.out.println("\nID sudah terpakai, silakan masukkan ID lain.");
                    break;

                case 2:
                if(members[0] == null){
                    System.out.println("\nTidak ada member terdaftar.");
                } else { System.out.print("Masukkan ID member yang ingin dihapus: "); String idmem = sc.next();
                boolean found = false;
                for(int i = 0 ; i < members.length ; i++){
                    if(members[i] != null && idmem.equalsIgnoreCase(members[i].getIdmember())){ 
                        found = true;
                        System.out.println("\nMember berhasil dihapus.");
                        for(int j = i ; j < members.length - 1 ; j++){
                            members[j] = members[j+1];
                        } members[members.length - 1] = null;
                        break;
                    }
                } if(!found){
                    System.out.printf("\nTidak ada member dengan ID %s, silakan masukkan ulang.", idmem);
                }
                } break;

                case 3:
                int index = 1;
                    if(members[0] == null){
                        System.out.println("\nTidak ada member terdaftar.");
                    } else { System.out.println("\n----------------------------------\n   DAFTAR MEMBER MINIMARKET\n----------------------------------");
                    for(Member i : members){
                        if(i == null){
                        break;
                    } else System.out.print(index + ". ");
                    System.out.println(i.displayMember());
                    System.out.println("----------------------------------");
                    index++;
                }
            } break;

                case 0: menu(); break;

                default:System.out.println("\nPilihan yang Anda masukkan tidak ada dalam daftar, Silakan input ulang.\n"); aksesMember();
            }
        }while(aksesD);
        sc.close();
    }
}
