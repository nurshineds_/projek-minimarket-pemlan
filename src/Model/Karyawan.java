package Model;

public class Karyawan {
    private String idKaryawan;
    private String namaKaryawan;
    private double gaji;

    //basic setter dan getter
    public String getIdKaryawan() {return idKaryawan;}
    public void setIdKaryawan(String idKaryawan) {this.idKaryawan = idKaryawan;}
    public double getGaji() {return gaji;}
    public void setGaji(double gaji) {this.gaji = gaji;}
    public String getNamaKaryawan() {return namaKaryawan;}
    public void setNamaKaryawan(String namaKaryawan) {this.namaKaryawan = namaKaryawan;}

    //kostruktor untuk menambahkan karyawan
    public Karyawan(String ID, String nama, double gaji){
        setIdKaryawan(ID);
        setNamaKaryawan(nama);
        setGaji(gaji);
    }

    //method untuk menampilkan data karyawan
    public String displayKaryawan(){
        String display = "";
        display += "ID Karyawan: " + this.idKaryawan + "\n   Nama Karyawan: " + this.namaKaryawan + "\n   Gaji Karyawan: " + this.gaji;
        return display;
    }
}
