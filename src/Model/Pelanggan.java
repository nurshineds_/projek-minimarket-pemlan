package Model;

public class Pelanggan {
    private String namaPelanggan;

    //basic setter dan getter
    public String getNamaPelanggan() {return namaPelanggan;}
    public void setNamaPelanggan(String namaPelanggan) {this.namaPelanggan = namaPelanggan;}

    //method untuk menampilkan nama pelanggan yang digunakan pada display class member
    public String pelanggan(){
        return "Nama: " + namaPelanggan;
    }
}
