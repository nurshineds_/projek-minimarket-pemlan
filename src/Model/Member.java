package Model;

public class Member extends Pelanggan{
    private String idmember;
    private String emailMember;
    private String telpMember;

    //basic setter dan getter
    public String getIdmember() {return idmember;}
    public void setIdmember(String idmember) {this.idmember = idmember;}
    public String getEmailMember() {return emailMember;}
    public void setEmailMember(String emailMember) {this.emailMember = emailMember;}
    public String getTelpMember() {return telpMember;}
    public void setTelpMember(String telpMember) {this.telpMember = telpMember;}

    //kostruktor untuk menambahkan member
    public Member(String nama, String ID, String email, String number){
        setNamaPelanggan(nama);
        setIdmember(ID);
        setEmailMember(email);
        setTelpMember(number);
    }

    //method untuk menampilkan data member, bagian nama dipanggil dari class Pelanggan yang merupakan parent
    public String displayMember(){
        String display = "";
        display += "ID Member: " + this.idmember + "\n   " + super.pelanggan() + "\n   Email: " + this.emailMember + "\n   Nomor Telp: " + this.telpMember;
        return display;
    }
}
