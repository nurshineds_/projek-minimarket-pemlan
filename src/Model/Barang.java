package Model;

public class Barang {
    private String kodeBarang;
    private String namaBarang;
    private double hargaBarang;
    private int stokBarang;

    //basic setter dan getter method
    public String getKodeBarang() {return kodeBarang;}
    public void setKodeBarang(String kodeBarang) {this.kodeBarang = kodeBarang;}
    public String getNamaBarang() {return namaBarang;}
    public void setNamaBarang(String namaBarang) {this.namaBarang = namaBarang;}
    public double getHargaBarang() {return hargaBarang;}
    public void setHargaBarang(double hargaBarang) {this.hargaBarang = hargaBarang;}
    public int getStokBarang() {return stokBarang;}
    public void setStokBarang(int stokBarang) {this.stokBarang = stokBarang;}

    public Barang(){}

    //kostruktor yang digunakan untuk menambahkan barang baru
    public Barang(String kode, String nama, double harga, int stok){
        setKodeBarang(kode);
        setNamaBarang(nama);
        setHargaBarang(harga);
        setStokBarang(stok);
    }

    //method untuk menambah stok barang
    public void tambahStok(int plusStok){
        stokBarang += plusStok;
    }

    //method untuk mengurangi stok barang
    public void kurangStok(int minStok){
        stokBarang -= minStok;
    }

    //method untuk menampilkan data dari barang
    public String displayBarang(){
        String display = "";
        display += "Kode Barang : " + this.kodeBarang + "\n   Nama Barang : " + this.namaBarang + "\n   Harga Barang: " + this.hargaBarang + "\n   Stok Barang : " + this.stokBarang;
        return display;
    }
}
