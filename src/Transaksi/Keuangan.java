package Transaksi;

public class Keuangan {
    private double kasAwal;
    private double kasMasuk;
    private double totalKas;

    //basic setter dan getter
    public double getKasAwal() {return kasAwal;}
    public void setKasAwal(double kasAwal) {this.kasAwal = kasAwal;}
    public double getKasMasuk() {return kasMasuk;}
    public void setKasMasuk(double kasMasuk) {this.kasMasuk = kasMasuk;}
    public double getTotalKas() {return totalKas;}
    public void setTotalKas(double totalKas) {this.totalKas = totalKas;}

    //method untuk menghitung total kas, total kas dihasilkan dari kas awal dan ditambah dengan uang yang masuk dari transaksi
    public double hitungKas(){
        totalKas += (kasAwal + kasMasuk);
        return totalKas;
    }
}
