package Transaksi;

public class Transaction {
    private String tanggal;
    private String namaKasir;
    private String namaPelanggan;
    private double totaltransaksi;

    //basic setter dan getter
    public double getTotaltransaksi() {return totaltransaksi;}
    public void setTotaltransaksi(double totaltransaksi) {this.totaltransaksi = totaltransaksi;}
    public String getTanggal() {return tanggal;}
    public void setTanggal(String tanggal) {this.tanggal = tanggal;}
    public String getNamaKasir() {return namaKasir;}
    public void setNamaKasir(String namaKasir) {this.namaKasir = namaKasir;}
    public String getNamaPelanggan() {return namaPelanggan;}
    public void setNamaPelanggan(String namaPelanggan) {this.namaPelanggan = namaPelanggan;}

    //kostruktor untuk membuat transaksi baru
    public Transaction(String tanggal, String namaKasir, String pelanggan, double total){
        setTanggal(tanggal);
        setNamaKasir(namaKasir);
        setNamaPelanggan(pelanggan);
        setTotaltransaksi(total);
    }

    //method untuk menampilkan detail transaksi
    public String diplayTransaksi(){
        String display = "";
        display += "   Tanggal transaksi: " + this.tanggal + "\n   Kasir: " + this.namaKasir + "\n   Pelanggan: " + this.namaPelanggan + "\n   Nominal transaksi: " + this.totaltransaksi;
        return display;
    }
}
